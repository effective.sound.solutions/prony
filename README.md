# Prony expansion

This is the homepage of a method for approximation of an arbitrary continuous function in the neighborhood of a given point by complex exponential functions (exponentials). The method is similar to the Taylor series to the effect that approximation is performed by the derivatives at the given point. Determination of parameters of the exponentials is made using Prony’s method.

## References

A short description of the method is given in

*E. Azarov, M. Vashkevich and A. Petrovsky, "Instantaneous parameters estimation algorithm for noisy AM-FM oscillatory signals," 2014 22nd European Signal Processing Conference (EUSIPCO), 2014, pp. 989-993.*

*Azarov, E.S., Vashkevich, M.I., Petrovsky, A.A. Estimation of the instantaneous signal parameters using a modified Prony’s method. Aut. Control Comp. Sci. 49, 110–121 (2015). https://doi.org/10.3103/S0146411615020029*
