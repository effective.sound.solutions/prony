function vals = Taylor_expansion( derivs, args, point )

args = args(:)';
vals = zeros(size(args));

for n=1:length(derivs)
    vals = vals + derivs(n)*(args - point).^(n-1)/factorial(n-1);
end

end

