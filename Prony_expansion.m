function [A,omega,theta,alpha]=Prony_expansion(S) % S - data sequence

p=length(S)/2;

Right=S(p+1:end);
Left=zeros(p,p);

for N=1:p
    Left(N,:)=S(p+N-1:-1:N);
end

CurrRank=rank(Left);
if(CurrRank<p)
    if(p>=2)
        [A,omega,theta,alpha]=Prony_expansion(S(1:end-2));
        A=[A; 0]; omega=[omega; 0]; theta=[theta; 0]; alpha=[alpha; 0];
    else
        A=zeros(size(Right));
        omega=zeros(size(Right));
        theta=zeros(size(Right));
        alpha=zeros(size(Right));
    end
    return;
end
 
fprintf('Rank - %f\n',CurrRank);

a=Left\(-Right);
s=roots([1 a.']);

omega=imag(s);
alpha=real(s);

z_mtx=zeros(p,p);
for N=1:p
    z_mtx(N,:)=s.^(N-1).';
end

h=z_mtx\S(1:p);
A=abs(h);
theta=angle(h);

end