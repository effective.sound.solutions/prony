clc;

t0 = 1.0; % approximation point
n_deriv = 4; % number of derivatives including 0 order
t=-0.5:0.2:5; % approximation interval

syms x;
syms f(x);
f=log(1+x);

%calculating derivatives
df=[];

for N=1:n_deriv
    df(N)=limit(diff(f,N-1), t0);
end

df=df(:);

approx_Taylor = Taylor_expansion(df, t, t0);

[A,omega,theta,alpha]=Prony_expansion(df);
n_points=length(t);
n_cos=size(A,1);
Amp_r=repmat(A,1,n_points);
Frc_r=repmat(omega,1,n_points);
Phs_r=repmat(theta,1,n_points);
t_r=repmat(t-t0,n_cos,1);
Dmp_r=repmat(alpha,1,n_points);
approx_Prony=Amp_r.*exp((Dmp_r+1j*Frc_r).*t_r+1j*Phs_r);
approx_Prony=sum(approx_Prony,1);

Width=690;
Height=540;
Width=Width/2.6;
Height=Height/2.6;

%% Plotting a signal
true_vals = subs(f,t);

p2 = plot(t,true_vals,'b');
hold on;
p1 = plot(t,approx_Taylor,'x-.k'); % Taylor
grid on;
p3 = plot(t, real(approx_Prony), 'o--r'); % Prony
ylim_fun = get(gca,'YLim');

plot(t0,df(1),'.g','LineWidth',3,'MarkerSize',50);

ylim(ylim_fun);
ylim([-2.0 4.0]*1);
xlim([min(t) max(t)]);
title([ num2str(n_deriv-1) ' derivatives']);
xLab1 = xlabel('$$x$$');
yLab1 = ylabel('$$f$$($$x$$)');
hLeg1 = legend('$$\log(1+x)$$','Taylor','Prony','Location','northwest');

set([hLeg1 xLab1 yLab1],'Interpreter','latex','FontSize',14);
set([p1 p2 p3],'LineWidth', 1.5);

